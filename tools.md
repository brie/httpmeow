# 🛠️  Tools 🛠️

## Prepping the Cats

  - **Midjourney** - The cat images on this site were all generated via [Midjourney](https://midjourney.com/app). Prompts are issued via [Discord](https://discord.com).
  - **ObjGen** - The [beta version](https://beta5.objgen.com/) of the [JSON Generator](https://beta5.objgen.com/json/local/design) by ObjGen was very useful for planning out the structure of `data/prompts.json`. 

## Serving and Deploying the Cats

### Serve

  - **Python Flask**
  - **Gunicorn**

 ### Deploy

  - **GitLab** - The source code is stored in a `git` repository.
  - **Google Cloud Build** - I use Google Cloud Build to bundle the Flask app into a container image. See the [Dockerfile](Dockerfile). 
  - **Google Cloud Run** - The image is deployed to and served by Google Cloud Run. 
  - **Render** - [Render](https://render.com) is a unified cloud to build and run all your apps and websites with free TLS certificates, a global CDN, DDoS protection, private networks, and auto deploys from Git. It is quite nice for development but I deploy to Google Cloud Run for production.

## Styling the Cats

  - **Skeleton** - The [super simple responsive CSS boilerplate](http://getskeleton.com/) was really useful and easy to use.

## Testing the Cats

  - **Flask2Postman**
  - **pytest**

```shell
flask2postman app.app --name "HTTP Catus Codes" --folders > collection.json
```
