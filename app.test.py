import unittest
from app import app
class BasicTestCase(unittest.TestCase):


# INDEX
#######

    def test_index(self):
        tester = app.test_client(self)
        response = tester.get('/', content_type='html/text')
        self.assertEqual(response.status_code, 200)
        # self.assertEqual(response.data, b'Hello World!')
 
 # ABOUT
 #######

    def test_about(self):
        #        request.user_agent="Hello/v0"
        tester = app.test_client(self)
        response = tester.get('/about', content_type='html/text')
        self.assertEqual(response.status_code, 200)
        # print(type(response))
        # print(response.request.user_agent)
        # self.assertEqual(response.data, b'Hello World!')
 
# Test teapot cat
##################

    def test_http418_cat(self):
        tester = app.test_client(self)
        response = tester.get('/418', content_type='html/text')
        self.assertEqual(response.status_code, 200)
        self.assertIn(b'teapot', response.data)

# Test custom 404
#################

    def test_404(self):
        tester = app.test_client(self)
        response = tester.get('/no', content_type='html/text')
        self.assertEqual(response.status_code, 404)
        self.assertIn(b'MEOW?', response.data)

if __name__ == '__main__':
    unittest.main()