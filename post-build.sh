#!/bin/bash

# Miscellaneous post-build tasks

flask2postman app.app --name "HTTP Catus Codes" -b http://localhost:8000  --folders > dev-collection.json
flask2postman app.app --name "HTTP Catus Codes" -b https://catsilove.com  --folders > prod-collection.json
