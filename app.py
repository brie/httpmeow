import json
import os
from flask import Flask, request, render_template

app = Flask(__name__)

@app.route("/")
def index():
    '''Show the home page'''
    return render_template("index.html")

@app.route("/about")
def about():
    '''Show the about page'''
    return render_template("about.html")

@app.route("/class")
def pageclass():
    '''Under development'''
    return render_template("index-with-class.html")

@app.route("/random")
def show_random_cat():
    '''When complete, get random code from what's available'''
    code = "418"
    # look up that random code
    try:
        codetext, prompt = get_codetext_prompt(code)
    except KeyError:
        print("You get a 404, actually.")
        return render_template('404.html'), 404
    return render_template("cats.html", code=code, codetext=codetext, prompt=prompt)

@app.errorhandler(404)
def page_not_found(e):
    '''404 - what can I say?'''
    # note that we set the 404 status explicitly
    return render_template('404.html'), 404

@app.errorhandler(500)
def internal_server_error(e):
    '''Errors happen'''
    # note that we set the 404 status explicitly
    return render_template('500.html'), 500

@app.route("/1xx")
def shsow_class():
    '''Under development'''
    return render_template("1xx.html")

@app.route('/<int:variable>', methods=['GET'])
def get_cat(variable):
    '''The brains of the operation, 
       this is what turns any integer passed after the / and returns a cat'''
    code = str(variable)
    codetext = str()
    prompt = str()
    try:
        codetext, prompt = get_codetext_prompt(code)
    except KeyError:
        print("You get a 404, actually.")
        return render_template('404.html'), 404
    return render_template("cats.html", code=code, codetext=codetext, prompt=prompt)

def get_codetext_prompt(code):
    '''Open the JSON file and get the information you need. 
       Close the file after yourself on the way out'''
    prompt_file = open('data/prompts.json')
    data = json.load(prompt_file)
    codetext = data[code]['codetext']
    prompt = data[code]['prompt']
    # Closing file
    prompt_file.close()
    return codetext, prompt

if __name__ == "__main__":
    app.directory='./'
    app.run(host="0.0.0.0", port=int(os.environ.get("PORT", 8080)))
