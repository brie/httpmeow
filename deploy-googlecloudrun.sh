#!/bin/bash


GCR_IMAGE=$(cat .env)

gcloud builds submit --tag  ${GCR_IMAGE} ${PWD}
gcloud run deploy httpmeow --image ${GCR_IMAGE}
